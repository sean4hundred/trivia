Template.search.helpers({
    friended: function () {
        if(Meteor.user().profile) return Meteor.user().profile.friends.indexOf(this._id) + 1;
    },
    isYou: function () {
       return Meteor.userId() === this._id;
    },
   usersIndex: () => UserIndex
});

Template.search.events({
  "click .addfriend": function (){
     Meteor.users.update(Meteor.userId(), {
        $addToSet: {
           "profile.friends": this._id
        }
     });
  }
});